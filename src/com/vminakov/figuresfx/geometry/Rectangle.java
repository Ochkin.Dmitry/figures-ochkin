package com.vminakov.figuresfx.geometry;

import javafx.scene.paint.Color;

public class Rectangle extends Figure {
    private double width;
    private double height;

    private Rectangle(double centerX, double centerY, double lineWidth, Color color) {
        super(FIGURE_TYPE_RECTANGLE, centerX, centerY, lineWidth, color);
    }

    public Rectangle(double centerX, double centerY, double lineWidth, Color color, double width, double height) {
        this(centerX, centerY, lineWidth, color);
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double getSquare() {
        return width * height;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Rectangle{");
        sb.append("height=").append(height);
        sb.append(", centerX=").append(centerX);
        sb.append(", centerY=").append(centerY);
        sb.append(", lineWidth=").append(lineWidth);
        sb.append(", color=").append(color);
        sb.append('}');
        return sb.toString();
    }
}
