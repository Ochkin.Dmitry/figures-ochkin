package com.vminakov.figuresfx.geometry;

import javafx.scene.paint.Color;

public class Circle extends Figure {
    private double radius;

    private Circle(double centerX, double centerY, double lineWidth, Color color) {
        super(FIGURE_TYPE_CIRCLE, centerX, centerY, lineWidth, color);
    }

    public Circle(double centerX, double centerY, double lineWidth, Color color, double radius) {
        this(centerX, centerY, lineWidth, color);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getSquare() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Circle{");
        sb.append("radius=").append(radius);
        sb.append(", centerX=").append(centerX);
        sb.append(", centerY=").append(centerY);
        sb.append(", lineWidth=").append(lineWidth);
        sb.append(", color=").append(color);
        sb.append('}');
        return sb.toString();
    }
}
