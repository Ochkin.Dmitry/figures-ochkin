package com.vminakov.figuresfx.geometry;

import javafx.scene.paint.Color;

public abstract class Figure {
    public static final int FIGURE_TYPE_CIRCLE      = 0;
    public static final int FIGURE_TYPE_RECTANGLE   = 1;
    public static final int FIGURE_TYPE_TRIANGLE    = 2;

    private int type;
    protected double centerX;
    protected double centerY;
    protected double lineWidth;
    protected Color color;

    public Figure(int type, double centerX, double centerY, double lineWidth, Color color) {
        this.type = type;
        this.centerX = centerX;
        this.centerY = centerY;
        this.lineWidth = lineWidth;
        this.color = color;
    }

    public double getCenterX() {
        return centerX;
    }

    public void setCenterX(double centerX) {
        this.centerX = centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public void setCenterY(double centerY) {
        this.centerY = centerY;
    }

    public double getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getType() {
        return type;
    }

    public abstract double getSquare();
}
