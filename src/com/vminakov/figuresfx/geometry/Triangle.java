package com.vminakov.figuresfx.geometry;

import javafx.scene.paint.Color;

public class Triangle extends Figure {
    private double base;

    private Triangle(double centerX, double centerY, double lineWidth, Color color) {
        super(FIGURE_TYPE_TRIANGLE, centerX, centerY, lineWidth, color);
    }

    public Triangle(double centerX, double centerY, double lineWidth, Color color, double base) {
        this(centerX, centerY, lineWidth, color);
        this.base = base;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    @Override
    public double getSquare() {
        return base * base / 2;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Triangle{");
        sb.append("base=").append(base);
        sb.append(", centerX=").append(centerX);
        sb.append(", centerY=").append(centerY);
        sb.append(", lineWidth=").append(lineWidth);
        sb.append(", color=").append(color);
        sb.append('}');
        return sb.toString();
    }
}
