package com.vminakov.figuresfx;

import com.vminakov.figuresfx.geometry.Circle;
import com.vminakov.figuresfx.geometry.Figure;
import com.vminakov.figuresfx.geometry.Rectangle;
import com.vminakov.figuresfx.geometry.Triangle;
import javafx.scene.paint.Color;

import java.util.Random;

public class App {
    private Random rnd = new Random(System.currentTimeMillis());

    public static void main(String[] args) {
        new App().runTheApp();
    }

    private void runTheApp() {
        int tmp = rnd.nextInt(51);
        int figuresCount = tmp == 0 ? 1 : tmp;
        Figure[] figures = new Figure[figuresCount];
        for (int i = 0; i < figures.length; i++) {
            figures[i] = createFigure();
        }
        for (Figure figure : figures) {
            System.out.println("======================================================");

            System.out.println(figure.toString());
            System.out.printf("SQUARE = %.2f\n", figure.getSquare());

            System.out.println("======================================================");
        }
    }

    private Figure createFigure() {
        Figure figure = null;
        int type = rnd.nextInt(3);
        switch (type) {
            case Figure.FIGURE_TYPE_CIRCLE:
                int radius = rnd.nextInt(51);
                figure = new Circle(0.0, 0.0, 1.0, Color.CHARTREUSE, radius == 0 ? 10 : radius);
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                int w = rnd.nextInt(101);
                int h = rnd.nextInt(101);
                figure = new Rectangle(0.0, 0.0, 1.0, Color.CORNFLOWERBLUE, w == 0 ? 10 : w, h == 0 ? 10 : h);
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                int base = rnd.nextInt(101);
                figure = new Triangle(0.0, 0.0, 1.0, Color.CYAN, base == 0 ? 10 : base);
                break;
            default:
                System.out.println("unknown figure type!");
        }
        return figure;
    }
}
